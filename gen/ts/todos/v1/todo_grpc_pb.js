// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var todos_v1_todo_pb = require('../../todos/v1/todo_pb.js');

function serialize_todos_v1_CreateTodoRequest(arg) {
  if (!(arg instanceof todos_v1_todo_pb.CreateTodoRequest)) {
    throw new Error('Expected argument of type todos.v1.CreateTodoRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_todos_v1_CreateTodoRequest(buffer_arg) {
  return todos_v1_todo_pb.CreateTodoRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_todos_v1_CreateTodoResponse(arg) {
  if (!(arg instanceof todos_v1_todo_pb.CreateTodoResponse)) {
    throw new Error('Expected argument of type todos.v1.CreateTodoResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_todos_v1_CreateTodoResponse(buffer_arg) {
  return todos_v1_todo_pb.CreateTodoResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_todos_v1_ReadTodosRequest(arg) {
  if (!(arg instanceof todos_v1_todo_pb.ReadTodosRequest)) {
    throw new Error('Expected argument of type todos.v1.ReadTodosRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_todos_v1_ReadTodosRequest(buffer_arg) {
  return todos_v1_todo_pb.ReadTodosRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_todos_v1_ReadTodosResponse(arg) {
  if (!(arg instanceof todos_v1_todo_pb.ReadTodosResponse)) {
    throw new Error('Expected argument of type todos.v1.ReadTodosResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_todos_v1_ReadTodosResponse(buffer_arg) {
  return todos_v1_todo_pb.ReadTodosResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_todos_v1_StreamTodosRequest(arg) {
  if (!(arg instanceof todos_v1_todo_pb.StreamTodosRequest)) {
    throw new Error('Expected argument of type todos.v1.StreamTodosRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_todos_v1_StreamTodosRequest(buffer_arg) {
  return todos_v1_todo_pb.StreamTodosRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_todos_v1_StreamTodosResponse(arg) {
  if (!(arg instanceof todos_v1_todo_pb.StreamTodosResponse)) {
    throw new Error('Expected argument of type todos.v1.StreamTodosResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_todos_v1_StreamTodosResponse(buffer_arg) {
  return todos_v1_todo_pb.StreamTodosResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var TodoService = exports.TodoService = {
  createTodo: {
    path: '/todos.v1.Todo/CreateTodo',
    requestStream: false,
    responseStream: false,
    requestType: todos_v1_todo_pb.CreateTodoRequest,
    responseType: todos_v1_todo_pb.CreateTodoResponse,
    requestSerialize: serialize_todos_v1_CreateTodoRequest,
    requestDeserialize: deserialize_todos_v1_CreateTodoRequest,
    responseSerialize: serialize_todos_v1_CreateTodoResponse,
    responseDeserialize: deserialize_todos_v1_CreateTodoResponse,
  },
  readTodos: {
    path: '/todos.v1.Todo/ReadTodos',
    requestStream: false,
    responseStream: false,
    requestType: todos_v1_todo_pb.ReadTodosRequest,
    responseType: todos_v1_todo_pb.ReadTodosResponse,
    requestSerialize: serialize_todos_v1_ReadTodosRequest,
    requestDeserialize: deserialize_todos_v1_ReadTodosRequest,
    responseSerialize: serialize_todos_v1_ReadTodosResponse,
    responseDeserialize: deserialize_todos_v1_ReadTodosResponse,
  },
  streamTodos: {
    path: '/todos.v1.Todo/StreamTodos',
    requestStream: false,
    responseStream: true,
    requestType: todos_v1_todo_pb.StreamTodosRequest,
    responseType: todos_v1_todo_pb.StreamTodosResponse,
    requestSerialize: serialize_todos_v1_StreamTodosRequest,
    requestDeserialize: deserialize_todos_v1_StreamTodosRequest,
    responseSerialize: serialize_todos_v1_StreamTodosResponse,
    responseDeserialize: deserialize_todos_v1_StreamTodosResponse,
  },
};

exports.TodoClient = grpc.makeGenericClientConstructor(TodoService);
