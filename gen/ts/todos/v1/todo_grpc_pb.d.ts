// package: todos.v1
// file: todos/v1/todo.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as todos_v1_todo_pb from "../../todos/v1/todo_pb";

interface ITodoService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    createTodo: ITodoService_ICreateTodo;
    readTodos: ITodoService_IReadTodos;
    streamTodos: ITodoService_IStreamTodos;
}

interface ITodoService_ICreateTodo extends grpc.MethodDefinition<todos_v1_todo_pb.CreateTodoRequest, todos_v1_todo_pb.CreateTodoResponse> {
    path: "/todos.v1.Todo/CreateTodo";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<todos_v1_todo_pb.CreateTodoRequest>;
    requestDeserialize: grpc.deserialize<todos_v1_todo_pb.CreateTodoRequest>;
    responseSerialize: grpc.serialize<todos_v1_todo_pb.CreateTodoResponse>;
    responseDeserialize: grpc.deserialize<todos_v1_todo_pb.CreateTodoResponse>;
}
interface ITodoService_IReadTodos extends grpc.MethodDefinition<todos_v1_todo_pb.ReadTodosRequest, todos_v1_todo_pb.ReadTodosResponse> {
    path: "/todos.v1.Todo/ReadTodos";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<todos_v1_todo_pb.ReadTodosRequest>;
    requestDeserialize: grpc.deserialize<todos_v1_todo_pb.ReadTodosRequest>;
    responseSerialize: grpc.serialize<todos_v1_todo_pb.ReadTodosResponse>;
    responseDeserialize: grpc.deserialize<todos_v1_todo_pb.ReadTodosResponse>;
}
interface ITodoService_IStreamTodos extends grpc.MethodDefinition<todos_v1_todo_pb.StreamTodosRequest, todos_v1_todo_pb.StreamTodosResponse> {
    path: "/todos.v1.Todo/StreamTodos";
    requestStream: false;
    responseStream: true;
    requestSerialize: grpc.serialize<todos_v1_todo_pb.StreamTodosRequest>;
    requestDeserialize: grpc.deserialize<todos_v1_todo_pb.StreamTodosRequest>;
    responseSerialize: grpc.serialize<todos_v1_todo_pb.StreamTodosResponse>;
    responseDeserialize: grpc.deserialize<todos_v1_todo_pb.StreamTodosResponse>;
}

export const TodoService: ITodoService;

export interface ITodoServer extends grpc.UntypedServiceImplementation {
    createTodo: grpc.handleUnaryCall<todos_v1_todo_pb.CreateTodoRequest, todos_v1_todo_pb.CreateTodoResponse>;
    readTodos: grpc.handleUnaryCall<todos_v1_todo_pb.ReadTodosRequest, todos_v1_todo_pb.ReadTodosResponse>;
    streamTodos: grpc.handleServerStreamingCall<todos_v1_todo_pb.StreamTodosRequest, todos_v1_todo_pb.StreamTodosResponse>;
}

export interface ITodoClient {
    createTodo(request: todos_v1_todo_pb.CreateTodoRequest, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.CreateTodoResponse) => void): grpc.ClientUnaryCall;
    createTodo(request: todos_v1_todo_pb.CreateTodoRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.CreateTodoResponse) => void): grpc.ClientUnaryCall;
    createTodo(request: todos_v1_todo_pb.CreateTodoRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.CreateTodoResponse) => void): grpc.ClientUnaryCall;
    readTodos(request: todos_v1_todo_pb.ReadTodosRequest, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.ReadTodosResponse) => void): grpc.ClientUnaryCall;
    readTodos(request: todos_v1_todo_pb.ReadTodosRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.ReadTodosResponse) => void): grpc.ClientUnaryCall;
    readTodos(request: todos_v1_todo_pb.ReadTodosRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.ReadTodosResponse) => void): grpc.ClientUnaryCall;
    streamTodos(request: todos_v1_todo_pb.StreamTodosRequest, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<todos_v1_todo_pb.StreamTodosResponse>;
    streamTodos(request: todos_v1_todo_pb.StreamTodosRequest, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<todos_v1_todo_pb.StreamTodosResponse>;
}

export class TodoClient extends grpc.Client implements ITodoClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public createTodo(request: todos_v1_todo_pb.CreateTodoRequest, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.CreateTodoResponse) => void): grpc.ClientUnaryCall;
    public createTodo(request: todos_v1_todo_pb.CreateTodoRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.CreateTodoResponse) => void): grpc.ClientUnaryCall;
    public createTodo(request: todos_v1_todo_pb.CreateTodoRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.CreateTodoResponse) => void): grpc.ClientUnaryCall;
    public readTodos(request: todos_v1_todo_pb.ReadTodosRequest, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.ReadTodosResponse) => void): grpc.ClientUnaryCall;
    public readTodos(request: todos_v1_todo_pb.ReadTodosRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.ReadTodosResponse) => void): grpc.ClientUnaryCall;
    public readTodos(request: todos_v1_todo_pb.ReadTodosRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: todos_v1_todo_pb.ReadTodosResponse) => void): grpc.ClientUnaryCall;
    public streamTodos(request: todos_v1_todo_pb.StreamTodosRequest, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<todos_v1_todo_pb.StreamTodosResponse>;
    public streamTodos(request: todos_v1_todo_pb.StreamTodosRequest, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<todos_v1_todo_pb.StreamTodosResponse>;
}
