import {
    Server, ServerCredentials, ServerUnaryCall, sendUnaryData,
    ServerWritableStream
} from '@grpc/grpc-js';
import {
    CreateTodoRequest, CreateTodoResponse,
    ReadTodosRequest, ReadTodosResponse, TodoItem,
    StreamTodosRequest, StreamTodosResponse,
} from '../gen/ts/todos/v1/todo_pb';
import { TodoService } from '../gen/ts/todos/v1/todo_grpc_pb';

let todos: TodoItem[] = [];

function createTodo(
    call: ServerUnaryCall<CreateTodoRequest, CreateTodoResponse>,
    callback: sendUnaryData<CreateTodoResponse>
) {
    console.log(call);
    const response = new CreateTodoResponse();
    const newTodo = new TodoItem();
    newTodo.setText(call.request.getText());
    newTodo.setId(todos.length + 1);
    todos.push(newTodo);
    console.info('New Todo received: ', newTodo);

    response.setTodoItem(newTodo);
    callback(null, response);
}

function readTodos(
    _call: ServerUnaryCall<ReadTodosRequest, ReadTodosResponse>,
    callback: sendUnaryData<ReadTodosResponse>
) {
    const response = new ReadTodosResponse();
    response.setItemsList(todos);
    callback(null, response);
}

function streamTodos(
    call: ServerWritableStream<StreamTodosRequest, StreamTodosResponse>,
) {
    if (call.writable) {
        todos.forEach(todo => {
            call.write(new StreamTodosResponse().setTodoItem(todo))
        });
    }
    call.end();
}

const server = new Server();
server.addService(TodoService, { createTodo, readTodos, streamTodos });

const PORT = '3001';
server.bindAsync(`0.0.0.0:${PORT}`, ServerCredentials.createInsecure(), () => {
    server.start();
    console.log(`Server is running an 0.0.0.0:${PORT}`);
});

