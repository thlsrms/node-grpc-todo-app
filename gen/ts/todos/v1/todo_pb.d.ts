// package: todos.v1
// file: todos/v1/todo.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class TodoItem extends jspb.Message { 
    getId(): number;
    setId(value: number): TodoItem;
    getText(): string;
    setText(value: string): TodoItem;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TodoItem.AsObject;
    static toObject(includeInstance: boolean, msg: TodoItem): TodoItem.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TodoItem, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TodoItem;
    static deserializeBinaryFromReader(message: TodoItem, reader: jspb.BinaryReader): TodoItem;
}

export namespace TodoItem {
    export type AsObject = {
        id: number,
        text: string,
    }
}

export class CreateTodoRequest extends jspb.Message { 
    getText(): string;
    setText(value: string): CreateTodoRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CreateTodoRequest.AsObject;
    static toObject(includeInstance: boolean, msg: CreateTodoRequest): CreateTodoRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CreateTodoRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CreateTodoRequest;
    static deserializeBinaryFromReader(message: CreateTodoRequest, reader: jspb.BinaryReader): CreateTodoRequest;
}

export namespace CreateTodoRequest {
    export type AsObject = {
        text: string,
    }
}

export class CreateTodoResponse extends jspb.Message { 

    hasTodoItem(): boolean;
    clearTodoItem(): void;
    getTodoItem(): TodoItem | undefined;
    setTodoItem(value?: TodoItem): CreateTodoResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CreateTodoResponse.AsObject;
    static toObject(includeInstance: boolean, msg: CreateTodoResponse): CreateTodoResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CreateTodoResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CreateTodoResponse;
    static deserializeBinaryFromReader(message: CreateTodoResponse, reader: jspb.BinaryReader): CreateTodoResponse;
}

export namespace CreateTodoResponse {
    export type AsObject = {
        todoItem?: TodoItem.AsObject,
    }
}

export class ReadTodosRequest extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ReadTodosRequest.AsObject;
    static toObject(includeInstance: boolean, msg: ReadTodosRequest): ReadTodosRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ReadTodosRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ReadTodosRequest;
    static deserializeBinaryFromReader(message: ReadTodosRequest, reader: jspb.BinaryReader): ReadTodosRequest;
}

export namespace ReadTodosRequest {
    export type AsObject = {
    }
}

export class ReadTodosResponse extends jspb.Message { 
    clearItemsList(): void;
    getItemsList(): Array<TodoItem>;
    setItemsList(value: Array<TodoItem>): ReadTodosResponse;
    addItems(value?: TodoItem, index?: number): TodoItem;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ReadTodosResponse.AsObject;
    static toObject(includeInstance: boolean, msg: ReadTodosResponse): ReadTodosResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ReadTodosResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ReadTodosResponse;
    static deserializeBinaryFromReader(message: ReadTodosResponse, reader: jspb.BinaryReader): ReadTodosResponse;
}

export namespace ReadTodosResponse {
    export type AsObject = {
        itemsList: Array<TodoItem.AsObject>,
    }
}

export class StreamTodosRequest extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StreamTodosRequest.AsObject;
    static toObject(includeInstance: boolean, msg: StreamTodosRequest): StreamTodosRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StreamTodosRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StreamTodosRequest;
    static deserializeBinaryFromReader(message: StreamTodosRequest, reader: jspb.BinaryReader): StreamTodosRequest;
}

export namespace StreamTodosRequest {
    export type AsObject = {
    }
}

export class StreamTodosResponse extends jspb.Message { 

    hasTodoItem(): boolean;
    clearTodoItem(): void;
    getTodoItem(): TodoItem | undefined;
    setTodoItem(value?: TodoItem): StreamTodosResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StreamTodosResponse.AsObject;
    static toObject(includeInstance: boolean, msg: StreamTodosResponse): StreamTodosResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StreamTodosResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StreamTodosResponse;
    static deserializeBinaryFromReader(message: StreamTodosResponse, reader: jspb.BinaryReader): StreamTodosResponse;
}

export namespace StreamTodosResponse {
    export type AsObject = {
        todoItem?: TodoItem.AsObject,
    }
}
