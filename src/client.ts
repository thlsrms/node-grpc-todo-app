import { credentials } from "@grpc/grpc-js";
import {
    CreateTodoRequest, ReadTodosRequest,
    StreamTodosRequest, StreamTodosResponse
} from "../gen/ts/todos/v1/todo_pb";
import { TodoClient } from "../gen/ts/todos/v1/todo_grpc_pb";

function exitOnError(error: Error) {
    console.error(error);
    process.exit(1);
}

const HOST = '127.0.0.1';
const PORT = '3001';

const client: TodoClient = new TodoClient(
    `${HOST}:${PORT}`,
    credentials.createInsecure()
);
client.waitForReady(Date.now() + 10000, (error) => {
    if (error) exitOnError(error);

    console.log('Client connected to' + `${HOST}:${PORT}`);
});

function newTodo(task: string) {
    const createTodoReq = new CreateTodoRequest();
    createTodoReq.setText(task);

    client.createTodo(createTodoReq, (error, response) => {
        if (error) exitOnError(error);

        console.log('Todo Created');
        console.info(response.getTodoItem());
    });
}

// Dummy data
const tasks = [
    'Walk the dog',
    'Do laundry',
    'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim.',
    'blah blah'
]
tasks.forEach(t => newTodo(t));

client.readTodos(new ReadTodosRequest(), (error, response) => {
    if (error) exitOnError(error);

    console.log('Listing Todos:');
    console.info(response.getItemsList());
});

// Stream
const call = client.streamTodos(new StreamTodosRequest());
call.on('data', (response: StreamTodosResponse) => {
    console.info("Data received: " + response.getTodoItem());
});

call.on('end', () => console.info('Server closed connection.'));
